import 'package:Luchapedia/bio_fighter.dart';
import 'package:flutter/material.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      color: Colors.purpleAccent[700], //Los íconos de la app tendran esos colores (es como un tema)
      title: 'Material App',
      home: BioFighter(),
    );
  }
}
